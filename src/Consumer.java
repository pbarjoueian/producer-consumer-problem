
class Consumer extends Thread {

    private Buffer buffer;
    private int number;

    public Consumer(Buffer c, int number) {
        buffer = c;
        this.number = number;
    }

    public void run() {
        int value = 0;
        for (int i = 0; i < 100; i++) {
            value = buffer.get();
            System.out.println("Consumer #"
                    + this.number
                    + " got: " + value);
        }
    }
}