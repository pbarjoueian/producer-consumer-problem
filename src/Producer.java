
import java.util.Random;

class Producer extends Thread {

    private Buffer buffer;
    private int number;
    Random rnd;

    public Producer(Buffer b, int number) {
        buffer = b;
        this.number = number;
    }

    public void run() {
        rnd = new Random();
        for (int counter = 0; counter < 100; counter++) {
            int i = rnd.nextInt(100);
            buffer.put(i);
            System.out.println("Producer #" + this.number
                    + " put: " + i);
            try {
                sleep((int) (Math.random() * 100));
            } catch (InterruptedException e) {
            }
        }
    }
}
